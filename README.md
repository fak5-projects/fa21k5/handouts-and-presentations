# Handouts and Presentations

All handouts and presentations from 2021.

* Intro presentations covering needed information about each group.
* Cheatsheets / handouts serving as a quick reference about each subtopic.
* Outro presentations covering outlooks or deeper explanations about what was done.
